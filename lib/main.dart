import 'package:demo_app/Screens/home.dart';
import 'package:demo_app/Screens/sign_up_screen.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Constants/globals.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const GoogleSignIn(),
    );
  }
}

class GoogleSignIn extends StatefulWidget {
  const GoogleSignIn({Key? key}) : super(key: key);
  @override
  _GoogleSignInState createState() => _GoogleSignInState();
}

class _GoogleSignInState extends State<GoogleSignIn> {
  bool? isloggedout;

  getSignIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final bool? loggedvalue = prefs.getBool('isloggedout');

    if (loggedvalue == null) {
      setState(() {
        isloggedout = true;
      });
    } else {
      setState(() {
        isloggedout = loggedvalue;
      });
    }
  }

  getLanguage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final bool? languagevalue = prefs.getBool('isArabic');

    if (languagevalue == null) {
      setState(() {
        isarabic = false;
      });
    } else {
      setState(() {
        isarabic = languagevalue;
      });
    }
  }

  @override
  void initState() {
    getSignIn();
    getLanguage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: (isloggedout == true) ? const SignUpScreen() : const Home(),
    );
  }
}
