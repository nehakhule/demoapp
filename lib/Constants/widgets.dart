import 'package:flutter/material.dart';

final leftEditIcon = Container(
  color: Colors.green,
  child: const Icon(Icons.edit),
  alignment: Alignment.centerLeft,
);
final rightDeleteIcon1 = Container(
  color: Colors.red,
  child: const Text('Delete',
      textAlign: TextAlign.center,
      style: TextStyle(
          fontSize: 24, fontFamily: 'Gilroy', fontWeight: FontWeight.bold)),
  alignment: Alignment.centerRight,
);
final rightDeleteIcon2 = Container(
  color: Colors.red,
  child: const Text('حذف',
      textAlign: TextAlign.center,
      style: TextStyle(
          fontSize: 24, fontFamily: 'Gilroy', fontWeight: FontWeight.bold)),
  alignment: Alignment.centerRight,
);
