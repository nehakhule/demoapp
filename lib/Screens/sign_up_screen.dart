import 'package:demo_app/Constants/responsive_size.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  static final GoogleSignIn googleSignIn = GoogleSignIn();
  @override
  State<SignUpScreen> createState() => _SignUpScreenState();

  static void signOutGoogle() async {
    await googleSignIn.signOut();
  }
}

class _SignUpScreenState extends State<SignUpScreen> {
  final FirebaseAuth auth = FirebaseAuth.instance;

  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.lightGreen,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'DEMO APP',
                style: TextStyle(
                    fontSize: 45,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: screenHeight(context) * 0.1,
              ),
              SizedBox(
                width: screenWidth(context) * 0.8,
                child: MaterialButton(
                    child: (!isloading)
                        ? const Text(
                            "Sign In By Google",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                        : const CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.black)),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: const BorderSide(
                          color: Colors.black,
                        )),
                    onPressed: () {
                      setState(() {
                        isloading = true;
                      });
                      signup(context);
                    }),
              )
            ],
          ),
        ));
  }

  Future<void> signup(BuildContext context) async {
    final GoogleSignInAccount? googleSignInAccount =
        await SignUpScreen.googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;
      final AuthCredential authCredential = GoogleAuthProvider.credential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);

      UserCredential result = await auth.signInWithCredential(authCredential);
      User? user = result.user;
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      String? username = user!.displayName;
      await prefs.setString('username', username!);

      await prefs.setBool('isloggedout', false);

      // ignore: unnecessary_null_comparison
      if (result != null) {
        setState(() {
          isloading = false;
        });
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => const Home()));
      }
    }
  }
}
