import 'package:demo_app/Screens/home.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Constants/globals.dart';
import '../Constants/responsive_size.dart';

class LanguageChange extends StatefulWidget {
  const LanguageChange({Key? key}) : super(key: key);

  @override
  State<LanguageChange> createState() => _LanguageChangeState();
}

class _LanguageChangeState extends State<LanguageChange> {
  Object? val = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => Navigator.pop(context)),
        elevation: 0,
        backgroundColor: Colors.green,
        title: (isarabic == true)
            ? const Text(
                "إلغاء",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.bold),
              )
            : const Text(
                "Change Language",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.bold),
              ),
      ),
      body: Column(
        children: [
          ListTile(
            title: const Text("English",
                style: TextStyle(
                    fontSize: 20.0,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.bold)),
            leading: Radio(
              toggleable: true,
              value: 1,
              groupValue: val,
              onChanged: (value) {
                setState(() {
                  val = value;
                });
              },
              activeColor: Colors.green,
            ),
          ),
          ListTile(
            title: const Text("Arabic",
                style: TextStyle(
                    fontSize: 20.0,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.bold)),
            leading: Radio(
              value: 2,
              groupValue: val,
              onChanged: (value) {
                setState(() {
                  val = value;
                });
              },
              activeColor: Colors.green,
            ),
          ),
          SizedBox(
            height: screenHeight(context) * 0.04,
          ),
          SizedBox(
            width: screenWidth(context) * 0.6,
            child: MaterialButton(
              color: Colors.lightGreen,
              child: (isarabic == true)
                  ? const Text('أضف',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Gilroy',
                          fontWeight: FontWeight.bold))
                  : const Text('Save',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Gilroy',
                          fontWeight: FontWeight.bold)),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  side: const BorderSide(
                    color: Colors.black,
                  )),
              onPressed: () async {
                if (val == 2) {
                  setState(() {
                    isarabic = true;
                  });
                  final SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  await prefs.setBool('isArabic', true);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Home()),
                  );
                } else if (val == 1) {
                  setState(() {
                    isarabic = false;
                  });
                  final SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  await prefs.setBool('isArabic', false);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Home()),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
