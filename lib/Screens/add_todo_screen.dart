import 'package:date_time_picker/date_time_picker.dart';
import 'package:demo_app/Constants/responsive_size.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

import '../Constants/globals.dart';

class DialogBox extends StatefulWidget {
  const DialogBox({
    Key? key,
    required this.addList,
  }) : super(key: key);

  final Function(String title, String des) addList;

  @override
  State<DialogBox> createState() => _DialogBoxState();
}

class _DialogBoxState extends State<DialogBox> {
  final TextEditingController _title = TextEditingController();
  final TextEditingController datetimecontroller = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _title.dispose();
    datetimecontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => Navigator.pop(context)),
        elevation: 0,
        backgroundColor: Colors.green,
        title: const Text(
          "Add Task",
          style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
              fontFamily: 'Gilroy',
              fontWeight: FontWeight.bold),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _title,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    hintText: (isarabic == true) ? 'اسم المهمة' : 'Task Name',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DateTimePicker(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please select date';
                    }
                    return null;
                  },
                  cursorColor: const Color(0xFF75101D),
                  type: DateTimePickerType.dateTime,
                  controller: datetimecontroller,
                  lastDate: DateTime(2030),
                  icon: const Icon(Icons.event),
                  dateLabelText:
                      (isarabic == true) ? ' التاريخ و الوقت' : 'Date and Time',
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    hintText: (isarabic == true)
                        ? ' التاريخ و الوقت'
                        : 'Date and Time',
                  ),
                  onChanged: (val) {
                    var now = DateTime.parse(val);
                    var todayDate =
                        DateFormat("dd/MM/yyyy    hh:mm a").format(now);

                    setState(() {
                      datetimecontroller.text = todayDate;
                    });
                  },
                  firstDate: DateTime(2000),
                ),
              ),
              SizedBox(
                height: screenHeight(context) * 0.04,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: screenWidth(context) * 0.4,
                    child: MaterialButton(
                      color: Colors.lightGreen,
                      child: (isarabic == true)
                          ? const Text('أضف',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Gilroy',
                                  fontWeight: FontWeight.bold))
                          : const Text('Save',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Gilroy',
                                  fontWeight: FontWeight.bold)),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: const BorderSide(
                            color: Colors.black,
                          )),
                      onPressed: () {
                        if (!_formKey.currentState!.validate()) {
                        } else {
                          widget.addList(_title.text, datetimecontroller.text);
                          Fluttertoast.showToast(
                              msg: 'Task Added',
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: Colors.lightGreen,
                              textColor: Colors.white);
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context) * 0.4,
                    child: MaterialButton(
                      child: (isarabic == true)
                          ? const Text('واضح',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Gilroy',
                                  fontWeight: FontWeight.bold))
                          : const Text('Clear',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Gilroy',
                                  fontWeight: FontWeight.bold)),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: const BorderSide(
                            color: Colors.black,
                          )),
                      onPressed: () {
                        setState(() {
                          _title.clear();
                          datetimecontroller.clear();
                        });
                      },
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
