import 'package:date_time_picker/date_time_picker.dart';
import 'package:demo_app/Constants/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Constants/widgets.dart';
import '../model/todolist.dart';
import 'add_todo_screen.dart';

class TodoScreen extends StatefulWidget {
  const TodoScreen({Key? key}) : super(key: key);

  @override
  State<TodoScreen> createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  List<TodoList> listTodo = [];

  void deletelist(TodoList list) {
    setState(() {
      listTodo.remove(list);
    });
    Fluttertoast.showToast(
        msg: 'Task Deleted',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.lightGreen,
        textColor: Colors.white);
  }

  void cancellist(TodoList list) {
    setState(() {
      listTodo.add(list);
    });
    Fluttertoast.showToast(
        msg: 'Task not Deleted',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.lightGreen,
        textColor: Colors.white);
  }

  Future updatelist(BuildContext context, TodoList oldtask) {
    TextEditingController _newtitle =
        TextEditingController(text: oldtask.title);
    TextEditingController _newdatetime =
        TextEditingController(text: oldtask.datetime);
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Column(
              children: const [
                Text('Edit a task to your list',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold)),
                Divider(
                  thickness: 2,
                ),
              ],
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: TextFormField(
                    controller: _newtitle,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      hintText: 'Task Name',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: DateTimePicker(
                    validator: (val) => val == null ? "Please pick date" : null,
                    cursorColor: const Color(0xFF75101D),
                    type: DateTimePickerType.dateTime,
                    controller: _newdatetime,
                    lastDate: DateTime(2030),
                    icon: const Icon(Icons.event),
                    dateLabelText: 'Date and Time',
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      hintText: 'Date and Time',
                    ),
                    onChanged: (val) {
                      var now = DateTime.parse(val);
                      var todayDate =
                          DateFormat("dd/MM/yyyy    hh:mm a").format(now);

                      setState(() {
                        _newdatetime.text = todayDate;
                      });
                    },
                    firstDate: DateTime(2000),
                  ),
                ),
              ],
            ),
            actions: <Widget>[
              MaterialButton(
                color: Colors.lightGreen,
                child: const Text('Update',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold)),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: const BorderSide(
                      color: Colors.black,
                    )),
                onPressed: () {
                  Navigator.of(context).pop();
                  editlist(
                    oldtask,
                    _newtitle.text,
                    _newdatetime.text,
                  );
                },
              ),
              MaterialButton(
                child: const Text('Cancel',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold)),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: const BorderSide(
                      color: Colors.black,
                    )),
                onPressed: () {
                  setState(() {
                    Navigator.of(context).pop();
                  });
                },
              )
            ],
          );
        });
  }

  void saveList(List<TodoList> list) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String encodedData = TodoList.encode(list);

    await prefs.setString('list', encodedData);
  }

  void addlist(String title, String datetime) {
    final list = TodoList(
      title: title,
      datetime: datetime,
    );
    setState(() {
      listTodo.add(list);
    });
    listTodo.sort((a, b) => b.datetime.compareTo(a.datetime));

    saveList(listTodo);
  }

  void editlist(TodoList oldtask, String newtitle, String newdatetime) {
    final list = TodoList(title: newtitle, datetime: newdatetime);
    setState(() {
      oldtask.title = list.title;
      oldtask.datetime = list.datetime;
    });
    listTodo.sort((a, b) => b.datetime.compareTo(a.datetime));
  }

  Future<dynamic> toAddScreen(BuildContext context) async {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => DialogBox(
                addList: (String title, String des) {
                  Navigator.of(context).pop();
                  addlist(
                    title,
                    des,
                  );
                },
              )),
    );
  }

  Future<dynamic> _displayDialog(BuildContext context, TodoList list) async {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title:
                (isarabic == true) ? const Text('حذف') : const Text('Delete?'),
            content: (isarabic == true)
                ? const Text('هل أنت متأكد أنك تريد حذف هذه المهمة؟')
                : const Text('Are you sure you want to delete this task?'),
            actions: [
              MaterialButton(
                child: (isarabic == true)
                    ? const Text('إلغاء', style: TextStyle(color: Colors.blue))
                    : const Text('Cancel',
                        style: TextStyle(color: Colors.blue)),
                onPressed: () {
                  Fluttertoast.showToast(
                      msg: 'Task not Deleted',
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.lightGreen,
                      textColor: Colors.white);
                  setState(() {
                    Navigator.of(context).pop();
                  });
                },
              ),
              MaterialButton(
                child: (isarabic == true)
                    ? const Text(
                        'حسنا',
                        style: TextStyle(color: Colors.blue),
                      )
                    : const Text('OK', style: TextStyle(color: Colors.blue)),
                onPressed: () {
                  deletelist(list);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: const RoundedRectangleBorder(
              side: BorderSide(color: Colors.black45, width: 1),
            ),
            elevation: 8,
            shadowColor: Colors.blue[100],
            margin: const EdgeInsets.only(top: 20, left: 20, right: 10),
            child: Dismissible(
              key: UniqueKey(),
              onDismissed: (DismissDirection direction) {
                if (direction == DismissDirection.endToStart) {
                  _displayDialog(context, listTodo[index]);
                } else if (direction == DismissDirection.startToEnd) {
                  updatelist(context, listTodo[index]);
                }
              },
              background: leftEditIcon,
              secondaryBackground:
                  (isarabic == true) ? rightDeleteIcon2 : rightDeleteIcon1,
              child: ListTile(
                title: Wrap(
                  alignment: WrapAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        listTodo[index].title,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        listTodo[index].datetime,
                        style:
                            const TextStyle(fontSize: 16, color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
        itemCount: listTodo.length,
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () => toAddScreen(context),
          tooltip: 'Add Task',
          child: const Icon(Icons.add)),
    );
  }
}
