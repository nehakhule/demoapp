import 'dart:async';

import 'package:demo_app/Constants/responsive_size.dart';
import 'package:flutter/material.dart';

class StopWatchScreen extends StatefulWidget {
  const StopWatchScreen({Key? key}) : super(key: key);

  @override
  _StopWatchScreenState createState() => _StopWatchScreenState();
}

class _StopWatchScreenState extends State<StopWatchScreen> {
  bool flag = true;
  Stream<int>? timerStream;
  late StreamSubscription<int> timerSubscription;
  String hoursStr = '00';
  String minutesStr = '00';
  String secondsStr = '00';

  late int oldTick;

  Stream<int> stopWatchStream() {
    late StreamController<int> streamController;
    Timer? timer;
    Duration timerInterval = const Duration(seconds: 1);
    int counter = 0;

    void stopTimer() {
      if (timer != null) {
        timer!.cancel();
        timer = null;
        counter = 0;
        streamController.close();
      }
    }

    void tick(_) {
      counter++;
      streamController.add(counter);

      if (!flag) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onPause: stopTimer,
      onResume: startTimer,
    );

    return streamController.stream;
  }

  getTime() {
    timerStream = stopWatchStream();
    timerSubscription = timerStream!.listen((int newTick) {
      setState(() {
        hoursStr =
            ((newTick / (60 * 60)) % 60).floor().toString().padLeft(2, '0');
        minutesStr = ((newTick / 60) % 60).floor().toString().padLeft(2, '0');
        secondsStr = (newTick % 60).floor().toString().padLeft(2, '0');

        oldTick = newTick;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: screenHeight(context) * 0.4,
              width: screenWidth(context) * 0.86,
              decoration: BoxDecoration(
                  color: Colors.lightGreen,
                  borderRadius: BorderRadius.circular(200)),
              child: Center(
                child: Text(
                  "$hoursStr:$minutesStr:$secondsStr",
                  style: const TextStyle(
                    fontSize: 75.0,
                  ),
                ),
              ),
            ),
            SizedBox(height: screenHeight(context) * 0.25),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                    onPressed: () {
                      timerSubscription.cancel();
                      timerStream = null;
                      setState(() {
                        hoursStr = '00';
                        minutesStr = '00';
                        secondsStr = '00';
                      });
                    },
                    icon: Image.asset("assets/images/stop-button.png")),
                IconButton(
                    onPressed: () {
                      getTime();
                    },
                    icon: Image.asset("assets/images/play-button.png")),
                IconButton(
                    onPressed: () {
                      timerSubscription.pause();
                    },
                    icon: Image.asset("assets/images/pause-button.png")),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
