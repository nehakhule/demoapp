import 'package:demo_app/Constants/globals.dart';
import 'package:demo_app/Constants/responsive_size.dart';
import 'package:demo_app/Screens/language_change.dart';
import 'package:demo_app/Screens/sign_up_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'encryption_decryption.dart';
import 'stopwatch_screen.dart';
import 'todo_screen.dart';

class Home extends StatefulWidget {
  const Home({
    Key? key,
  }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  late List<Widget> _widgetOptions;
  String? version;
  String? displayname;

  @override
  void initState() {
    homeTestState();
    getAppVersion();
    getUserName();

    super.initState();
  }

  homeTestState() {
    _widgetOptions = [
      const EncryptionScreen(),
      const TodoScreen(),
      const StopWatchScreen(),
    ];
  }

  getUserName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? username = prefs.getString('username');
    setState(() {
      displayname = username;
    });
  }

  getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appversion = packageInfo.version;
    setState(() {
      version = appversion;
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Drawer appDrawer() {
    return Drawer(
        child: ListView(
      children: [
        Container(
          height: screenHeight(context) * 0.13,
          color: Colors.green,
          child: const DrawerHeader(
            child: Center(
              child: Text(
                "DEMO APP",
                style: TextStyle(
                    fontSize: 30,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        ListTile(
          title: Center(
            child: (isarabic == true)
                ? const Text(
                    "إلغاء",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold),
                  )
                : const Text('Change Language',
                    style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold)),
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const LanguageChange()),
            );
          },
        ),
        const Divider(
          color: Colors.black,
        ),
        ListTile(
          title: Center(
            child: (isarabic == true)
                ? const Text(
                    " تسجيل خروج",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold),
                  )
                : const Text('Logout',
                    style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold)),
          ),
          onTap: () {
            showDialog<void>(
                context: context,
                builder: (BuildContext context) {
                  return CupertinoAlertDialog(
                    title: (isarabic == true)
                        ? const Text('حذف')
                        : const Text('Logout?'),
                    content: (isarabic == true)
                        ? const Text('هل أنت متأكد أنك تريد تسجيل الخروج؟')
                        : const Text('Are you sure you want to logout?'),
                    actions: [
                      MaterialButton(
                        child: (isarabic == true)
                            ? const Text('إلغاء')
                            : const Text('CANCEL'),
                        onPressed: () {
                          setState(() {
                            Navigator.of(context).pop();
                          });
                        },
                      ),
                      MaterialButton(
                        child: (isarabic == true)
                            ? const Text('حسنا')
                            : const Text('OK'),
                        onPressed: () async {
                          final SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          SignUpScreen.signOutGoogle();
                          await prefs.setBool('isloggedout', true);

                          setState(() {
                            isarabic = false;
                          });

                          Fluttertoast.showToast(
                              msg: 'Logged Out Succesfully!',
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              backgroundColor: Colors.white,
                              textColor: Colors.black);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const SignUpScreen()),
                          );
                        },
                      ),
                    ],
                  );
                });
          },
        ),
        const Divider(
          color: Colors.black,
        ),
        SizedBox(
          height: screenHeight(context) * 0.6,
        ),
        Container(
            alignment: Alignment.bottomCenter,
            child: Text(
              "App Version: $version",
            ))
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: (isarabic == true)
            ? Text(
                " أهلا بك  $displayname".toUpperCase(),
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.w700,
                ),
              )
            : Text(
                "Welcome  $displayname".toUpperCase(),
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.w700,
                ),
              ),
        centerTitle: true,
      ),
      drawer: appDrawer(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            child: _widgetOptions.elementAt(_selectedIndex),
          ),
        ],
      ),
      bottomNavigationBar: Card(
        elevation: 20,
        shadowColor: const Color(0xffFFFFFF),
        shape: const RoundedRectangleBorder(
          side: BorderSide(color: Color(0xffFFFFFF), width: 1),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        ),
        child: BottomNavigationBar(
            backgroundColor: const Color(0xffFFFFFF),
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Column(
                  children: [
                    Image.asset(
                      "assets/images/encryption.png",
                      height: 40,
                    ),
                    (isarabic == true)
                        ? const Text(
                            "التشفير",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                        : const Text(
                            "Encryption",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                  ],
                ),
                activeIcon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/images/encryption.png",
                      color: const Color(0xff53B175),
                      height: 40,
                    ),
                    (isarabic == true)
                        ? const Text(
                            "التشفير",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                        : const Text(
                            "Encryption",
                            style: TextStyle(
                                color: Color(0xff53B175),
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                  ],
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Column(
                  children: [
                    Image.asset(
                      "assets/images/to-do-list.png",
                      height: 40,
                    ),
                    (isarabic == true)
                        ? const Text(
                            "لكى يفعل",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                        : const Text(
                            "To-Do",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                  ],
                ),
                activeIcon: Column(
                  children: [
                    Image.asset(
                      "assets/images/to-do-list.png",
                      color: const Color(0xff53B175),
                      height: 40,
                    ),
                    (isarabic == true)
                        ? const Text(
                            "لكى يفعل",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                        : const Text(
                            "To-Do",
                            style: TextStyle(
                                color: Color(0xff53B175),
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                  ],
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Column(
                  children: [
                    Image.asset(
                      "assets/images/time-tracking.png",
                      height: 40,
                    ),
                    (isarabic == true)
                        ? const Text(
                            "ساعة التوقيف",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                        : const Text(
                            "Stop Watch",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                  ],
                ),
                activeIcon: Column(
                  children: [
                    Image.asset(
                      "assets/images/time-tracking.png",
                      height: 40,
                      color: const Color(0xff53B175),
                    ),
                    (isarabic == true)
                        ? const Text(
                            "ساعة التوقيف",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                        : const Text(
                            "Stop Watch",
                            style: TextStyle(
                                color: Color(0xff53B175),
                                fontFamily: 'Gilroy',
                                fontWeight: FontWeight.bold),
                          )
                  ],
                ),
                label: '',
              ),
            ],
            type: BottomNavigationBarType.shifting,
            currentIndex: _selectedIndex,
            selectedItemColor: Colors.black,
            iconSize: 24,
            onTap: _onItemTapped,
            elevation: 4),
      ),
    );
  }
}
