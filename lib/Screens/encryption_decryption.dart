import 'package:demo_app/Constants/globals.dart';
import 'package:demo_app/Constants/responsive_size.dart';
import 'package:encrypt/encrypt.dart' as en;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class EncryptionScreen extends StatefulWidget {
  const EncryptionScreen({Key? key}) : super(key: key);

  @override
  State<EncryptionScreen> createState() => _EncryptionScreenState();
}

class _EncryptionScreenState extends State<EncryptionScreen> {
  TextEditingController message = TextEditingController();
  TextEditingController secretKey = TextEditingController();
  final _formKey1 = GlobalKey<FormState>();

  String encrypttext = "";

  String decrypttext = "";
  bool? isencrypt;

  encryptData(String message, String secretKey) {
    final key = en.Key.fromUtf8(secretKey);
    final iv = en.IV.fromLength(16);
    final encrypter = en.Encrypter(en.AES(key));
    final encrypted = encrypter.encrypt(message, iv: iv);
    setState(() {
      encrypttext = encrypted.base64;
    });
  }

  decryptData(String message, String secretKey) {
    final key = en.Key.fromUtf8(secretKey);
    final iv = en.IV.fromLength(16);
    final encrypter = en.Encrypter(en.AES(key));
    final encrypted = encrypter.encrypt(message, iv: iv);
    final decrypted = encrypter.decrypt(encrypted, iv: iv);
    setState(() {
      decrypttext = decrypted;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Form(
        key: _formKey1,
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: message,
                    maxLines: 6,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      hintText: (isarabic == true) ? 'رسالة' : 'Message',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: secretKey,
                    maxLength: 32,
                    validator: (value) {
                      if (value!.length != 32) {
                        return 'Please enter 32 characters';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      hintText:
                          (isarabic == true) ? 'المفتاح السري' : 'Secret Key',
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      width: screenWidth(context) * 0.4,
                      child: MaterialButton(
                          color: Colors.lightGreen,
                          child: (isarabic == true)
                              ? const Text(
                                  'تشفير',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Gilroy',
                                      fontWeight: FontWeight.bold),
                                )
                              : const Text(
                                  'Encrypt',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Gilroy',
                                      fontWeight: FontWeight.bold),
                                ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: const BorderSide(
                                color: Colors.black,
                              )),
                          onPressed: () {
                            if (!_formKey1.currentState!.validate()) {
                            } else {
                              encryptData(message.text, secretKey.text);
                              setState(() {
                                isencrypt = true;
                              });
                            }
                          }),
                    ),
                    SizedBox(
                      width: screenWidth(context) * 0.4,
                      child: MaterialButton(
                          color: Colors.lightGreen,
                          child: (isarabic == true)
                              ? const Text(
                                  ' فك تشفير',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Gilroy',
                                      fontWeight: FontWeight.bold),
                                )
                              : const Text(
                                  'Decrypt',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Gilroy',
                                      fontWeight: FontWeight.bold),
                                ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: const BorderSide(
                                color: Colors.black,
                              )),
                          onPressed: () {
                            if (!_formKey1.currentState!.validate()) {
                            } else {
                              decryptData(message.text, secretKey.text);
                              setState(() {
                                isencrypt = false;
                              });
                            }
                          }),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context) * 0.1,
                ),
                (isencrypt == true)
                    ? GestureDetector(
                        onLongPress: () {
                          Clipboard.setData(ClipboardData(text: encrypttext));
                          Fluttertoast.showToast(
                              msg: 'Text copied to clipboard',
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              backgroundColor: Colors.lightGreen,
                              textColor: Colors.white);
                        },
                        child: Container(
                          height: screenHeight(context) * 0.3,
                          width: screenWidth(context) * 0.8,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                              color: Colors.grey.withOpacity(0.5),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("Encrypted String:",
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontFamily: 'Gilroy',
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(encrypttext,
                                      style: const TextStyle(
                                          color: Colors.black,
                                          fontFamily: 'Gilroy',
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : GestureDetector(
                        onLongPress: () {
                          Clipboard.setData(ClipboardData(text: decrypttext));
                          Fluttertoast.showToast(
                              msg: 'Text copied to clipboard',
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              backgroundColor: Colors.lightGreen,
                              textColor: Colors.white);
                        },
                        child: Container(
                          height: screenHeight(context) * 0.3,
                          width: screenWidth(context) * 0.8,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                              color: Colors.grey.withOpacity(0.5),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("Decrypted String:",
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontFamily: 'Gilroy',
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(decrypttext,
                                      style: const TextStyle(
                                          color: Colors.black,
                                          fontFamily: 'Gilroy',
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
