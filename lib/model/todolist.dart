import 'dart:convert';

class TodoList {
  String title;
  String datetime;

  TodoList({
    required this.title,
    required this.datetime,
  });

  factory TodoList.fromJson(Map<String, dynamic> json) {
    String title = json['title'];
    String datetime = json['datetime'];

    return TodoList(
      title: title,
      datetime: datetime,
    );
  }

  static Map<String, dynamic> toMap(TodoList todo) => {
        'title': todo.title,
        'datetime': todo.datetime,
      };

  static String encode(List<TodoList> todos) => json.encode(
        todos
            .map<Map<String, dynamic>>((todo) => TodoList.toMap(todo))
            .toList(),
      );

  static List<TodoList> decode(String todos) =>
      (json.decode(todos) as List<dynamic>)
          .map<TodoList>((item) => TodoList.fromJson(item))
          .toList();
}
